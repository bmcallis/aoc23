import { describe, expect, test } from 'vitest';
import { add, parseInput } from './common';

export const exampleInput = `
.....
.F-7.
.|.|.
.L-J.
.....
`;

describe('parse input', () => {
  test('should parse input', () => {
    expect(parseInput(exampleInput)).toEqual(['foo', 'bar']);
  });
});

describe('Common day utils', () => {
  test('basic test example', () => {
    expect(add(1, 1)).toEqual(2);
  });
});
