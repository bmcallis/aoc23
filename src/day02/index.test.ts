import { describe, expect, test } from 'vitest';
import { findMaxes, getGame, isPossible, maxPower, parseInput, part1, part2 } from './index';

describe('parse input', () => {
  test('should parse input', () => {
    const input = `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`;
    expect(parseInput(input)).toEqual([
      'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green',
      'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue',
      'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red',
      'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red',
      'Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green',
    ]);
  });
});

describe('helpers', () => {
  test('get game', () => {
    expect(getGame('Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green')).toEqual([
      '3 blue, 4 red',
      '1 red, 2 green, 6 blue',
      '2 green',
    ]);
    expect(getGame('Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue')).toEqual([
      '1 blue, 2 green',
      '3 green, 4 blue, 1 red',
      '1 green, 1 blue',
    ]);
    expect(
      getGame('Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red'),
    ).toEqual(['8 green, 6 blue, 20 red', '5 blue, 4 red, 13 green', '5 green, 1 red']);
    expect(
      getGame('Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red'),
    ).toEqual(['1 green, 3 red, 6 blue', '3 green, 6 red', '3 green, 15 blue, 14 red']);
    expect(getGame('Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green')).toEqual([
      '6 red, 1 blue, 3 green',
      '2 blue, 1 red, 2 green',
    ]);
  });
  test('find max colors', () => {
    expect(findMaxes(['3 blue, 4 red', '1 red, 2 green, 6 blue', '2 green'])).toEqual({
      red: 4,
      green: 2,
      blue: 6,
    });
    expect(findMaxes(['1 blue, 2 green', '3 green, 4 blue, 1 red', '1 green, 1 blue'])).toEqual({
      red: 1,
      green: 3,
      blue: 4,
    });
    expect(
      findMaxes(['8 green, 6 blue, 20 red', '5 blue, 4 red, 13 green', '5 green, 1 red']),
    ).toEqual({ red: 20, green: 13, blue: 6 });
    expect(
      findMaxes(['1 green, 3 red, 6 blue', '3 green, 6 red', '3 green, 15 blue, 14 red']),
    ).toEqual({ red: 14, green: 3, blue: 15 });
    expect(findMaxes(['6 red, 1 blue, 3 green', '2 blue, 1 red, 2 green'])).toEqual({
      red: 6,
      green: 3,
      blue: 2,
    });
  });
  test('is red possible', () => {
    expect(isPossible({ red: 4, green: 0, blue: 0 })).toBe(true);
    expect(isPossible({ red: 12, green: 0, blue: 0 })).toBe(true);
    expect(isPossible({ red: 40, green: 0, blue: 0 })).toBe(false);
  });
  test('is green possible', () => {
    expect(isPossible({ red: 0, green: 2, blue: 0 })).toBe(true);
    expect(isPossible({ red: 0, green: 13, blue: 0 })).toBe(true);
    expect(isPossible({ red: 0, green: 20, blue: 0 })).toBe(false);
  });
  test('is blue possible', () => {
    expect(isPossible({ red: 0, green: 0, blue: 6 })).toBe(true);
    expect(isPossible({ red: 0, green: 0, blue: 14 })).toBe(true);
    expect(isPossible({ red: 0, green: 0, blue: 60 })).toBe(false);
  });
  test('max power', () => {
    expect(maxPower({ red: 4, green: 2, blue: 6 })).toEqual(48);
    expect(maxPower({ red: 1, green: 3, blue: 4 })).toEqual(12);
    expect(maxPower({ red: 20, green: 13, blue: 6 })).toEqual(1560);
    expect(maxPower({ red: 14, green: 3, blue: 15 })).toEqual(630);
    expect(maxPower({ red: 6, green: 3, blue: 2 })).toEqual(36);
  });
});

describe('part 1', () => {
  test('from example', () => {
    const input = `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`;
    expect(part1(input)).toEqual(8);
  });
  test('from example 1', () => {
    const input = `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green`;
    expect(part1(input)).toEqual(1);
  });
  test('from example 2', () => {
    const input = `Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue`;
    expect(part1(input)).toEqual(1);
  });
  test('from example 3', () => {
    const input = `Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red`;
    expect(part1(input)).toEqual(0);
  });
  test('from example 4', () => {
    const input = `Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red`;
    expect(part1(input)).toEqual(0);
  });
  test('from example 5', () => {
    const input = `Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`;
    expect(part1(input)).toEqual(1);
  });
});

describe('part 2', () => {
  test('from example', () => {
    const input = `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`;
    expect(part2(input)).toEqual(2286);
  });
});
