import run from 'aocrunner';

interface IMaxes {
  red: number;
  green: number;
  blue: number;
}

const MAX: IMaxes = { red: 12, green: 13, blue: 14 };

export const parseInput = (rawInput: string) => rawInput.split('\n');
export const getGame = (line: string) =>
  line
    .split(':')[1]
    .split(';')
    .map((pull) => pull.trim());
export const findMaxes = (game: string[]): IMaxes => {
  return game.reduce<IMaxes>(
    (acc, pull) => {
      const red = pull.match(/(\d+) red/);
      const green = pull.match(/(\d+) green/);
      const blue = pull.match(/(\d+) blue/);
      acc.red = red ? Math.max(acc.red, Number.parseInt(red[1], 10)) : acc.red;
      acc.green = green ? Math.max(acc.green, Number.parseInt(green[1], 10)) : acc.green;
      acc.blue = blue ? Math.max(acc.blue, Number.parseInt(blue[1], 10)) : acc.blue;
      // console.log({pull, red: red?.[1], green: green?.[1], blue: blue?.[1], acc})
      return acc;
    },
    { red: 0, green: 0, blue: 0 },
  );
};

export const isPossible = (maxes: IMaxes) => {
  return maxes.red <= MAX.red && maxes.green <= MAX.green && maxes.blue <= MAX.blue;
};

export const part1 = (rawInput: string): number | undefined => {
  const input = parseInput(rawInput);
  return input.reduce((acc, line, idx) => {
    const maxes = findMaxes(getGame(line));
    if (isPossible(maxes)) {
      return acc + idx + 1;
    }

    return acc;
  }, 0);
};

export const maxPower = (maxes: IMaxes): number => {
  return maxes.red * maxes.green * maxes.blue;
};
export const part2 = (rawInput: string): number | undefined => {
  const input = parseInput(rawInput);
  return input.reduce((acc, line) => {
    const maxes = findMaxes(getGame(line));
    return acc + maxPower(maxes);
  }, 0);
};

run({
  part1: {
    solution: part1,
  },
  part2: {
    solution: part2,
  },
});
