import { determineType } from './part1';

type StringNumObj = { [key: string]: number };
export type HandBid = [string, number];

const CARDS: StringNumObj = {
  A: 14,
  K: 13,
  Q: 12,
  J: 1,
  T: 10,
  9: 9,
  8: 8,
  7: 7,
  6: 6,
  5: 5,
  4: 4,
  3: 3,
  2: 2,
};

export const parseInput = (rawInput: string): HandBid[] =>
  rawInput.split('\n').map((line) => {
    const parts = line.split(' ');
    return [parts[0], Number.parseInt(parts[1])];
  });

export const hasWilds = (hand: string) => {
  return hand.includes('J');
};
export const determineTypeWithWilds = (hand: string) => {
  const wildMatches = hand.match(/J/g);
  if (wildMatches) {
    const max: [string, number] = ['A', 0];
    const cards: StringNumObj = {};
    hand.split('').forEach((card) => {
      if (card === 'J') return;

      cards[card] = cards[card] ? cards[card] + 1 : 1;
      if (cards[card] > max[1]) {
        max[0] = card;
        max[1] = cards[card];
      }
    });

    return determineType(hand.replaceAll('J', max[0]));
  }

  return determineType(hand);
};

export const compareHands = (a: string, b: string): number => {
  const rankA = determineTypeWithWilds(a);
  const rankB = determineTypeWithWilds(b);

  if (rankA < rankB) {
    return -1;
  }
  if (rankA > rankB) {
    return 1;
  }

  const aCards = a.split('');
  const bCards = b.split('');
  let returnValue = 0;
  for (let i = 0; i < 5; i++) {
    if (CARDS[aCards[i]] < CARDS[bCards[i]]) {
      returnValue = -1;
      i = 5;
    }
    if (CARDS[aCards[i]] > CARDS[bCards[i]]) {
      returnValue = 1;
      i = 5;
    }
  }
  return returnValue;
};

export const sortHands2 = (hands: HandBid[]) => {
  return hands.sort((a, b) => {
    return compareHands(a[0], b[0]);
  });
};

export const part2 = (rawInput: string): number | undefined => {
  const hands = parseInput(rawInput);
  const sortedHands = sortHands2(hands);

  return sortedHands.reduce((total, hb, idx) => {
    return (total += hb[1] * (idx + 1));
  }, 0);
};
