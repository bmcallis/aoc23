import { describe, expect, test } from 'vitest';
import { part1, part2 } from './index';

describe('part 1', () => {
  test('from example', () => {
    const input = `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`;
    expect(part1(input)).toEqual(6440);
  });
});

describe.only('part 2', () => {
  test('from example', () => {
    const input = `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`;
    expect(part2(input)).toEqual(5905);
  });
});
