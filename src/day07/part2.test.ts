import { describe, expect, test } from 'vitest';
import {
  HandBid,
  compareHands,
  parseInput,
  determineTypeWithWilds,
  sortHands2,
  part2,
} from './part2';

describe('parse input', () => {
  test('should parse input', () => {
    const input = `32T3K 765
T55J5 684`;
    expect(parseInput(input)).toEqual([
      ['32T3K', 765],
      ['T55J5', 684],
    ]);
  });
});

describe('determine hand type', () => {
  test('find high card type', () => {
    expect(determineTypeWithWilds('32467')).toEqual(1);
  });
  test('find on pair type', () => {
    expect(determineTypeWithWilds('77654')).toEqual(2);
  });
  test('find two pair type', () => {
    expect(determineTypeWithWilds('TT998')).toEqual(3);
  });
  test('find three of a kind type', () => {
    expect(determineTypeWithWilds('TTT62')).toEqual(4);
  });
  test('find full house type', () => {
    expect(determineTypeWithWilds('QQQ77')).toEqual(5);
  });
  test('find four of a kind type', () => {
    expect(determineTypeWithWilds('KKKK4')).toEqual(6);
  });
  test('find five of a kind type', () => {
    expect(determineTypeWithWilds('AAAAA')).toEqual(7);
  });
});

describe('determine hand type with wilds', () => {
  test('find on pair type', () => {
    expect(determineTypeWithWilds('7J654')).toEqual(2);
  });
  test('find two pair type', () => {
    // can't have two pair if you have a wild, it'll be a three of a kind, or just one pair
  });
  test('find three of a kind type', () => {
    expect(determineTypeWithWilds('TTJ62')).toEqual(4);
  });
  test('find full house type', () => {
    expect(determineTypeWithWilds('QJQ77')).toEqual(5);
  });
  test('find four of a kind type', () => {
    expect(determineTypeWithWilds('QJJQ2')).toEqual(6);
  });
  test('find five of a kind type', () => {
    expect(determineTypeWithWilds('AAAJA')).toEqual(7);
    expect(determineTypeWithWilds('JJJJJ')).toEqual(7);
  });
});

describe('compare hands', () => {
  test('five of a kind is highest', () => {
    expect(compareHands('AAAAA', 'KKKK4')).toEqual(1);
    expect(compareHands('AAAAA', 'QQQ77')).toEqual(1);
    expect(compareHands('AAAAA', 'TTT62')).toEqual(1);
    expect(compareHands('AAAAA', 'TT998')).toEqual(1);
    expect(compareHands('AAAAA', '77654')).toEqual(1);
    expect(compareHands('AAAAA', '32467')).toEqual(1);
    expect(compareHands('KKKK4', 'AAAAA')).toEqual(-1);
    expect(compareHands('QQQ77', 'AAAAA')).toEqual(-1);
    expect(compareHands('TTT62', 'AAAAA')).toEqual(-1);
    expect(compareHands('TT998', 'AAAAA')).toEqual(-1);
    expect(compareHands('77654', 'AAAAA')).toEqual(-1);
    expect(compareHands('32467', 'AAAAA')).toEqual(-1);
  });
  test('then four of a kind', () => {
    expect(compareHands('KKKK4', 'QQQ77')).toEqual(1);
    expect(compareHands('KKKK4', 'TTT62')).toEqual(1);
    expect(compareHands('KKKK4', 'TT998')).toEqual(1);
    expect(compareHands('KKKK4', '77654')).toEqual(1);
    expect(compareHands('KKKK4', '32467')).toEqual(1);
    expect(compareHands('QQQ77', 'KKKK4')).toEqual(-1);
    expect(compareHands('TTT62', 'KKKK4')).toEqual(-1);
    expect(compareHands('TT998', 'KKKK4')).toEqual(-1);
    expect(compareHands('77654', 'KKKK4')).toEqual(-1);
    expect(compareHands('32467', 'KKKK4')).toEqual(-1);
  });
  test('then full house', () => {
    expect(compareHands('QQQ77', 'TTT62')).toEqual(1);
    expect(compareHands('QQQ77', 'TT998')).toEqual(1);
    expect(compareHands('QQQ77', '77654')).toEqual(1);
    expect(compareHands('QQQ77', '32467')).toEqual(1);
    expect(compareHands('TTT62', 'QQQ77')).toEqual(-1);
    expect(compareHands('TT998', 'QQQ77')).toEqual(-1);
    expect(compareHands('77654', 'QQQ77')).toEqual(-1);
    expect(compareHands('32467', 'QQQ77')).toEqual(-1);
  });
  test('then three of a kind', () => {
    expect(compareHands('TTT62', 'TT998')).toEqual(1);
    expect(compareHands('TTT62', '77654')).toEqual(1);
    expect(compareHands('TTT62', '32467')).toEqual(1);
    expect(compareHands('TT998', 'TTT62')).toEqual(-1);
    expect(compareHands('77654', 'TTT62')).toEqual(-1);
    expect(compareHands('32467', 'TTT62')).toEqual(-1);
  });
  test('then two pair', () => {
    expect(compareHands('TT998', '77654')).toEqual(1);
    expect(compareHands('TT998', '32467')).toEqual(1);
    expect(compareHands('77654', 'TT998')).toEqual(-1);
    expect(compareHands('32467', 'TT998')).toEqual(-1);
  });
  test('then one pair', () => {
    expect(compareHands('77654', '32467')).toEqual(1);
    expect(compareHands('32467', '77654')).toEqual(-1);
  });
  test('same type goes to higher card', () => {
    expect(compareHands('AAAAA', 'KKKKK')).toEqual(1);
    expect(compareHands('KKKKK', 'AAAAA')).toEqual(-1);

    expect(compareHands('KK677', 'KT99T')).toEqual(1);
    expect(compareHands('KT99T', 'KK677')).toEqual(-1);

    expect(compareHands('QQQ9A', 'T5595')).toEqual(1);
    expect(compareHands('T5595', 'QQQ9A')).toEqual(-1);

    expect(compareHands('33332', '2AAAA')).toEqual(1);
    expect(compareHands('2AAAA', '33332')).toEqual(-1);

    expect(compareHands('77888', '77788')).toEqual(1);
    expect(compareHands('77788', '77888')).toEqual(-1);
  });
});

test('sort hands', () => {
  const hands: HandBid[] = [
    ['32T3K', 765],
    ['T55J5', 684],
    ['KK677', 28],
    ['KTJJT', 220],
    ['QQQJA', 483],
  ];
  expect(sortHands2(hands)).toEqual([
    ['32T3K', 765],
    ['KK677', 28],
    ['T55J5', 684],
    ['QQQJA', 483],
    ['KTJJT', 220],
  ]);
});

describe('part 2', () => {
  test('from example', () => {
    const input = `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`;
    expect(part2(input)).toEqual(5905);
  });
});
