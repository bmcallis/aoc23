import run from 'aocrunner';
// import { parseInput, sortHands } from './part1';
// import { sortHands2 } from './part2.ts';

type StringNumObj = { [key: string]: number };
export type HandBid = [string, number];

const CARDS: StringNumObj = {
  A: 14,
  K: 13,
  Q: 12,
  J: 1,
  T: 10,
  9: 9,
  8: 8,
  7: 7,
  6: 6,
  5: 5,
  4: 4,
  3: 3,
  2: 2,
};
const TYPES: StringNumObj = {
  highCard: 1,
  onePair: 2,
  twoPair: 3,
  trips: 4,
  fullHouse: 5,
  quads: 6,
  fiver: 7,
};

export const parseInput = (rawInput: string): HandBid[] =>
  rawInput.split('\n').map((line) => {
    const parts = line.split(' ');
    return [parts[0], Number.parseInt(parts[1])];
  });

export const determineType = (hand: string) => {
  const cards: StringNumObj = {};
  hand.split('').forEach((card) => {
    cards[card] = cards[card] ? cards[card] + 1 : 1;
  });

  const counts = Object.values(cards);
  if (counts.length === 1) {
    return TYPES.fiver;
  }
  if (counts.length === 2) {
    if (counts.includes(4)) {
      return TYPES.quads;
    }
    return TYPES.fullHouse;
  }
  if (counts.length === 3) {
    if (counts.includes(3)) {
      return TYPES.trips;
    }
    return TYPES.twoPair;
  }
  if (counts.length === 4) {
    return TYPES.onePair;
  }
  if (counts.length === 5) {
    return TYPES.highCard;
  }
  return -1;
};

export const determineTypeWithWilds = (hand: string) => {
  const wildMatches = hand.match(/J/g);
  if (wildMatches) {
    const max: [string, number] = ['A', 0];
    const cards: StringNumObj = {};
    hand.split('').forEach((card) => {
      if (card === 'J') return;

      cards[card] = cards[card] ? cards[card] + 1 : 1;
      if (cards[card] > max[1]) {
        max[0] = card;
        max[1] = cards[card];
      }
    });

    return determineType(hand.replaceAll('J', max[0]));
  }

  return determineType(hand);
};

export const compareHands = (a: string, b: string): number => {
  const rankA = determineType(a);
  const rankB = determineType(b);

  if (rankA < rankB) {
    return -1;
  }
  if (rankA > rankB) {
    return 1;
  }

  const aCards = a.split('');
  const bCards = b.split('');
  let returnValue = 0;
  for (let i = 0; i < 5; i++) {
    if (CARDS[aCards[i]] < CARDS[bCards[i]]) {
      returnValue = -1;
      i = 5;
    }
    if (CARDS[aCards[i]] > CARDS[bCards[i]]) {
      returnValue = 1;
      i = 5;
    }
  }
  return returnValue;
};

export const compareHands2 = (a: string, b: string): number => {
  const rankA = determineTypeWithWilds(a);
  const rankB = determineTypeWithWilds(b);

  if (rankA < rankB) {
    return -1;
  }
  if (rankA > rankB) {
    return 1;
  }

  const aCards = a.split('');
  const bCards = b.split('');
  let returnValue = 0;
  for (let i = 0; i < 5; i++) {
    if (CARDS[aCards[i]] < CARDS[bCards[i]]) {
      returnValue = -1;
      i = 5;
    }
    if (CARDS[aCards[i]] > CARDS[bCards[i]]) {
      returnValue = 1;
      i = 5;
    }
  }
  return returnValue;
};

export const sortHands = (hands: HandBid[]) => {
  return hands.sort((a, b) => {
    return compareHands(a[0], b[0]);
  });
};

export const sortHands2 = (hands: HandBid[]) => {
  return hands.sort((a, b) => {
    return compareHands2(a[0], b[0]);
  });
};

export const part1 = (rawInput: string): number | undefined => {
  const hands = parseInput(rawInput);
  const sortedHands = sortHands(hands);

  return sortedHands.reduce((total, hb, idx) => {
    return (total += hb[1] * (idx + 1));
  }, 0);
};

export const part2 = (rawInput: string): number | undefined => {
  const hands = parseInput(rawInput);
  const sortedHands = sortHands2(hands);

  return sortedHands.reduce((total, hb, idx) => {
    return (total += hb[1] * (idx + 1));
  }, 0);
};

run({
  part1: {
    solution: part1,
  },
  part2: {
    solution: part2,
  },
});
