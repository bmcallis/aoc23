import { describe, expect, test } from 'vitest';
import {
  calculateDistances,
  findRecords,
  getValues,
  getValues2,
  parseInput,
  part1,
  part2,
} from './index';

describe('parse input', () => {
  test('should parse input', () => {
    const input = `Time:      7  15   30
Distance:  9  40  200`;
    expect(parseInput(input)).toEqual([
      [7, 9],
      [15, 40],
      [30, 200],
    ]);
  });
});

describe('helpers', () => {
  test('get line values', () => {
    expect(getValues('Time:      7  15   30')).toEqual([7, 15, 30]);
    expect(getValues('Distance:  9  40  200')).toEqual([9, 40, 200]);
  });
  test('calculate distances', () => {
    expect(calculateDistances(0)).toEqual([]);
    expect(calculateDistances(7)).toEqual([6, 10, 12, 12, 10, 6]);
  });
  test('find records', () => {
    expect(findRecords(9, [6, 10, 12, 12, 10, 6])).toEqual([10, 12, 12, 10]);
  });

  test('get line values 2', () => {
    expect(getValues2('Time:      7  15   30')).toEqual(71530);
    expect(getValues2('Distance:  9  40  200')).toEqual(940200);
  });
});

describe('part 1', () => {
  test('from example', () => {
    const input = `Time:      7  15   30
Distance:  9  40  200`;
    expect(part1(input)).toEqual(288);
  });
});

describe('part 2', () => {
  test('from example', () => {
    const input = `Time:      7  15   30
Distance:  9  40  200`;
    expect(part2(input)).toEqual(71503);
  });
});
