import run from 'aocrunner';

export const parseInput = (rawInput: string) => {
  const lines = rawInput.split('\n');
  const times = getValues(lines[0]);
  const distances = getValues(lines[1]);
  return times?.map((t, idx) => [t, distances?.[idx]]);
};
export const getValues = (line: string): number[] => {
  const matches = line.match(/(\d+)/g);
  if (!matches) return [];
  return matches.map((v) => Number.parseInt(v, 10));
};
export const calculateDistances = (raceLength: number): number[] => {
  const distances = [];
  for (let i = 1; i < raceLength; i++) {
    const speed = i;
    const time = raceLength - i;
    distances.push(speed * time);
  }

  return distances;
};
export const findRecords = (record: number, distances: number[]): number[] => {
  return distances.filter((d) => d > record);
};

export const part1 = (rawInput: string): number | undefined => {
  const input = parseInput(rawInput);

  return input.reduce((product, race) => {
    const distances = calculateDistances(race[0]);
    return product * findRecords(race[1], distances).length;
  }, 1);
};

export const parseInput2 = (rawInput: string) => {
  const lines = rawInput.split('\n');
  const time = getValues2(lines[0]);
  const distance = getValues2(lines[1]);
  return [[time, distance]];
};
export const getValues2 = (line: string): number => {
  const matches = line.match(/(\d+)/g);
  if (!matches) return 0;
  return Number.parseInt(
    matches.reduce((acc, val) => acc + val, ''),
    10,
  );
};
export const part2 = (rawInput: string): number | undefined => {
  const input = parseInput2(rawInput);

  return input.reduce((product, race) => {
    const distances = calculateDistances(race[0]);
    return product * findRecords(race[1], distances).length;
  }, 1);
};

run({
  part1: {
    solution: part1,
  },
  part2: {
    solution: part2,
  },
});
