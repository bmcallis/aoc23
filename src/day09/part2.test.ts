import { describe, expect, test } from 'vitest';
import { part2, previous } from './part2';
import { exampleInput } from './common.test';

describe('helpers', () => {
  test.each([
    [[0, 3, 6, 9, 12, 15], -3],
    [[1, 3, 6, 10, 15, 21], -1],
    [[10, 13, 16, 21, 30, 45], 5],
  ])('previous(%o) => %i', (nums, expected) => {
    expect(previous(nums)).toEqual(expected);
  });
  test.only('previous', () => {
    const nums = [10, 13, 16, 21, 30, 45];
    expect(previous(nums)).toEqual(5);
  });
});

describe('part 2', () => {
  test('from example', () => {
    expect(part2(exampleInput)).toEqual(2);
  });
});
