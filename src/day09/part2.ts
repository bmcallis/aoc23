import { allSameNum, findDiffs, lastArr, parseInput } from './common.js';

export const part2 = (rawInput: string): number | undefined => {
  return parseInput(rawInput).reduce((acc, line) => {
    return acc + previous(line);
  }, 0);
};

export const previous = (history: number[]): number => {
  const sequences = [] as number[][];
  do {
    const seq = sequences.length ? lastArr(sequences) : history;
    sequences.push(findDiffs(seq));
  } while (!allSameNum(sequences[sequences.length - 1]));

  sequences.reverse().forEach((seq, idx) => {
    if (idx > 0) {
      const prevSeq = sequences[idx - 1];
      sequences[idx].splice(0, 0, seq[0] - prevSeq[0]);
    }
  });
  return history[0] - lastArr(sequences)[0];
};
