import { describe, expect, test } from 'vitest';
import { allSameNum, allZeros, findDiffs, parseInput } from './common';

export const exampleInput = `
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
`;

describe('parse input', () => {
  test('should parse input', () => {
    expect(parseInput(exampleInput)).toEqual([
      [0, 3, 6, 9, 12, 15],
      [1, 3, 6, 10, 15, 21],
      [10, 13, 16, 21, 30, 45],
    ]);
  });
});

describe('Common day utils', () => {
  test.each([
    [[0, 0, 0], true],
    [[1, 2, 3], false],
  ])('allZeros(%o) => %s', (nums, expected) => {
    expect(allZeros(nums)).toEqual(expected);
  });

  test.each([
    [[0, 0, 0], true],
    [[4, 4, 4], true],
    [[1, 2, 3], false],
  ])('allSameNum(%o) => %s', (nums, expected) => {
    expect(allSameNum(nums)).toEqual(expected);
  });

  test.each([
    [
      [0, 3, 6, 9, 12, 15],
      [3, 3, 3, 3, 3],
    ],
    [
      [1, 3, 6, 10, 15, 21],
      [2, 3, 4, 5, 6],
    ],
  ])('findDiffs(%o) => %o', (nums, expected) => {
    expect(findDiffs(nums)).toEqual(expected);
  });
});
