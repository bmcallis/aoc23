import { allSameNum, findDiffs, lastArr, lastNum, parseInput } from './common.js';

export const part1 = (rawInput: string): number | undefined => {
  return parseInput(rawInput).reduce((acc, line) => {
    return acc + predict(line);
  }, 0);
};

export const predict = (history: number[]): number => {
  const sequences = [] as number[][];
  do {
    const seq = sequences.length ? lastArr(sequences) : history;
    sequences.push(findDiffs(seq));
  } while (!allSameNum(sequences[sequences.length - 1]));

  sequences.reverse().forEach((seq, idx) => {
    if (idx > 0) {
      const prevSeq = sequences[idx - 1];
      sequences[idx].push(lastNum(seq) + lastNum(prevSeq));
    }
  });
  return lastNum(history) + lastNum(lastArr(sequences));
};
