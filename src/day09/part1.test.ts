import { describe, expect, test } from 'vitest';
import { part1, predict } from './part1';
import { exampleInput } from './common.test';

describe('helpers', () => {
  test.each([
    [[0, 3, 6, 9, 12, 15], 18],
    [[1, 3, 6, 10, 15, 21], 28],
    [[10, 13, 16, 21, 30, 45], 68],
  ])('predict(%o) => %i', (nums, expected) => {
    expect(predict(nums)).toEqual(expected);
  });
});

describe('part 1', () => {
  test('from example', () => {
    expect(part1(exampleInput)).toEqual(114);
  });
});
