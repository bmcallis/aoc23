import { inputTo2DNumArray } from '../utils/index.js';

export const parseInput = (rawInput: string): number[][] => {
  return inputTo2DNumArray(rawInput);
};

export const allZeros = (values: number[]): boolean => {
  return !values.some((val) => val !== 0);
};

export const allSameNum = (values: number[]): boolean => {
  return !values.some((val) => val !== values[0]);
};

export const findDiffs = (values: number[]): number[] => {
  return values.reduce((diffs, val, idx) => {
    if (idx < values.length - 1) {
      diffs.push(values[idx + 1] - val);
    }
    return diffs;
  }, [] as number[]);
};

export const lastNum = (arr: number[]) => arr[arr.length - 1];
export const lastArr = (arr: number[][]) => arr[arr.length - 1];

