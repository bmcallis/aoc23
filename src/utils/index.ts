export const inputToArray = (rawInput: string): string[] => {
  return rawInput
    .split('\n')
    .map((line) => line.trim())
    .filter(Boolean);
};

export const lineToNumArray = (line: string): number[] => {
  return line
    .split(' ')
    .filter(Boolean)
    .map((num) => Number.parseInt(num));
};

export const inputTo2DNumArray = (rawInput: string): number[][] => {
  return inputToArray(rawInput).map(lineToNumArray);
};
