import { describe, expect, test } from 'vitest';
import { inputTo2DNumArray, inputToArray, lineToNumArray } from './index';

describe('Common utilities', () => {
  test('parse to array input should trim lines', () => {
    const input = `
      line 1
      line 2
      line 3
    `;
    expect(inputToArray(input)).toEqual(['line 1', 'line 2', 'line 3']);
  });

  test('parse line to number arrat', () => {
    const input = `1 4  5 6   9`;
    expect(lineToNumArray(input)).toEqual([1, 4, 5, 6, 9]);
  });

  test('parse to 2D number array', () => {
    const input = `
      1 2 3
      4 5 6
    `;
    expect(inputTo2DNumArray(input)).toEqual([
      [1, 2, 3],
      [4, 5, 6],
    ]);
  });
});
