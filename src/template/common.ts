import { inputToArray } from '../utils/index.js';

export const parseInput = (rawInput: string): string[] => {
  return inputToArray(rawInput);
};

export const add = (a: number, b: number): number => {
  return a + b;
};
