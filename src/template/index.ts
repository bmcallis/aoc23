import run from 'aocrunner';
import { part1 } from './part1.js';
import { part2 } from './part2.js';

run({
  part1: { solution: part1 },
  part2: { solution: part2 },
});
