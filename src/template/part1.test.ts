import { describe, expect, test } from 'vitest';
import { part1 } from './part1';
import { exampleInput } from './common.test';

describe('helpers', () => {
  test('test', () => {
    expect(1).toEqual(1);
  });
});

describe('part 1', () => {
  test('from example', () => {
    expect(part1(exampleInput)).toEqual(undefined);
  });
});
