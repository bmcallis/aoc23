import { describe, expect, test } from 'vitest';
import { part2 } from './part2';
import { exampleInput } from './common.test';

describe('helpers', () => {
  test('test', () => {
    expect(1).toEqual(1);
  });
});

describe('part 2', () => {
  test('from example', () => {
    expect(part2(exampleInput)).toEqual(undefined);
  });
});
