import { parseInput } from './common.js';

export const part2 = (rawInput: string): number | undefined => {
  return parseInput(rawInput).reduce((acc, line) => {
    if (line) return acc;
    return acc;
  }, undefined);
};
