import run from 'aocrunner';

export const parseInput = (rawInput: string): string[] => rawInput.split('\n');
export const getDigits = (line: string): RegExpMatchArray | null => {
  return line.match(/\d/g);
};
export const getTwoDigitNum = (nums: RegExpMatchArray | string[] | null): number => {
  if (!nums) return 0;

  const tens = Number.parseInt(nums[0]) * 10;
  const ones = Number.parseInt(nums[nums.length - 1]);
  return tens + ones;
};

export const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  const total = input.reduce((acc, line) => {
    return acc + getTwoDigitNum(getDigits(line));
  }, 0);

  return total;
};

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const numStrings = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
export const getDigits2 = (line: string): string[] | null => {
  let first = null;
  let last = null;

  for (let i = 0; i < line.length; i++) {
    const num = Number.parseInt(line[i], 10);
    if (numbers.includes(num)) {
      first = first ?? num;
      last = num;
    } else {
      const numFromString = numStrings.reduce<number | undefined>((acc, str, idx) => {
        if (acc) return acc;

        const rest = line.substring(i);
        if (rest.startsWith(str)) {
          return idx + 1;
        }
      }, undefined);
      if (numFromString) {
        first = first ?? numFromString;
        last = numFromString;
      }
    }
  }

  if (first && last) {
    return [first + '', last + ''];
  }
  return null;
};

export const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  return input.reduce<number>((acc, line) => {
    return acc + getTwoDigitNum(getDigits2(line));
  }, 0);
};

run({
  part1: {
    solution: part1,
  },
  part2: {
    solution: part2,
  },
});
