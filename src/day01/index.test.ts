import { describe, expect, test } from 'vitest';
import { getDigits, getDigits2, getTwoDigitNum, parseInput, part1, part2 } from './index';

describe('parse input', () => {
  test('should parse input', () => {
    expect(parseInput('foo')).toEqual(['foo']);
    const input = `1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet`;
    expect(parseInput(input)).toEqual(['1abc2', 'pqr3stu8vwx', 'a1b2c3d4e5f', 'treb7uchet']);
  });
});

describe('helpers', () => {
  test('should return digits', () => {
    expect(getDigits('1ab2')).toEqual(['1', '2']);
    expect(getDigits('1abc2')).toEqual(['1', '2']);
    expect(getDigits('pqr3stu8vwx')).toEqual(['3', '8']);
    expect(getDigits('a1b2c3d4e5f')).toEqual(['1', '2', '3', '4', '5']);
    expect(getDigits('treb7uchet')).toEqual(['7']);
  });

  test('should get two digit number', () => {
    expect(getTwoDigitNum(['1', '2'])).toEqual(12);
    expect(getTwoDigitNum(['3', '8'])).toEqual(38);
    expect(getTwoDigitNum(['1', '2', '3', '4', '5'])).toEqual(15);
    expect(getTwoDigitNum(['7'])).toEqual(77);
  });

  test('should return digits for part two', () => {
    expect(getDigits2('12')).toEqual(['1', '2']);
    expect(getDigits2('1ab2')).toEqual(['1', '2']);
    expect(getDigits2('pqr3stu8vwx')).toEqual(['3', '8']);
    expect(getDigits2('a1b2c3d4e5f')).toEqual(['1', '5']);
    expect(getDigits2('treb7uchet')).toEqual(['7', '7']);
    expect(getDigits2('onetwo')).toEqual(['1', '2']);
    expect(getDigits2('two1nine')).toEqual(['2', '9']);
    expect(getDigits2('eightwothree')).toEqual(['8', '3']);
    expect(getDigits2('abcone2threexyz')).toEqual(['1', '3']);
    expect(getDigits2('xtwone3four')).toEqual(['2', '4']);
    expect(getDigits2('4nineeightseven2')).toEqual(['4', '2']);
    expect(getDigits2('zoneight234')).toEqual(['1', '4']);
    expect(getDigits2('7pqrstsixteen')).toEqual(['7', '6']);
  });
});

describe('part 1', () => {
  test('from example', () => {
    const input = `1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet`;
    expect(part1(input)).toEqual(142);
  });
});

describe('part 2', () => {
  test('from example', () => {
    const input = `two1nine
    eightwothree
    abcone2threexyz
    xtwone3four
    4nineeightseven2
    zoneight234
    7pqrstsixteen`;
    expect(part2(input)).toEqual(281);
  });
});
